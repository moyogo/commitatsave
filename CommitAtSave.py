import subprocess, os, time
from vanilla import *
from mojo.events import addObserver, removeObserver
from AppKit import NSImage, NSImageNameRightFacingTriangleTemplate, NSImageNameStopProgressTemplate

imStart = NSImage.imageNamed_(NSImageNameRightFacingTriangleTemplate)
imStart.setSize_((10, 10))

imStop = NSImage.imageNamed_(NSImageNameStopProgressTemplate)
imStop.setSize_((10, 10))


class commitAtSave(object):
    def __init__(self):
        self.commitsCount = {}
        self.gitRepo = False
        if AllFonts() == []: return
        for font in AllFonts():
            head, tail = os.path.split(font.path)
            fontName = font.info.familyName + '-' + font.info.styleName
            if self.isInGitRepository(head):
                self.gitRepo = True
                self.commitsCount[fontName] = self.getNumberOfUnpushedCommits(font)
            else:
                self.commitsCount[fontName] = 0
        self.path = None
        self.font = None
        self.fontName = ''
        font = CurrentFont()
        if font != None:
            self.font = font
            head, tail = os.path.split(self.font.path)
            self.path = head
            self.fontName = self.font.info.familyName + '-' + self.font.info.styleName
        
        self.title = "Commit At Save"
        self.w = FloatingWindow((150, 120), self.title, closable=False)
        self.w.titleTextBox = TextBox((10, 10, -10, 20), self.fontName, alignment='center', sizeStyle='mini')
        self.w.startButton = ImageButton((10, 30, -10, 20), imageObject=imStart, callback=self.startSaveObserver, sizeStyle="small")
        self.w.startButton.getNSButton().setBordered_(False)
        self.w.stopButton = ImageButton((10, 30, -10, 20), imageObject=imStop, callback=self.stopSaveObserver, sizeStyle="small")
        self.w.stopButton.getNSButton().setBordered_(False)
        buttonTitle = "Push %s" % str(self.commitsCount[self.fontName])
        self.w.pushButton = Button((10, 60, -10, 20), buttonTitle, callback=self.gitPush, sizeStyle="small")
        self.w.closeButton = Button((10, -30, -10, 20), "Close", callback=self.closeWindow, sizeStyle="small")
        self.w.stopButton.show(False)
        self.w.pushButton.show(self.path!=None and self.font!=None and self.commitsCount[self.fontName]!=0)
        addObserver(self, "currentFontChanged", "fontBecameCurrent")
        addObserver(self, "newFontOpened", "fontDidOpen")
        self.w.open()
    
    def getNumberOfUnpushedCommits(self, font):
        head, tail = os.path.split(font.path)
        path = head
        count = 0
        try:
            for c in subprocess.call(['git', 'cherry', '-v' ], cwd=path):
                if c == '+':
                    count += 1 
            return count
        except:
            return count
        
    def isInGitRepository(self, path):
        if not '.git' in subprocess.check_output(['ls', '-a'], cwd=path):
            count = 0
            nbDir = len(path.split(os.sep))
            for i in range(nbDir):
                head, tail = os.path.split(path)
                path = head
                if not '.git' in subprocess.check_output(['ls', '-a'], cwd=path):
                    count += 1
                else:
                    break
            if count == nbDir:
                print 'Warning: this is not a GIT repository'
                return False
        return path
        
    def updateTitle(self):
        nbCommits = str(self.commitsCount[self.fontName])
        self.w.titleTextBox.set(self.fontName)
        self.w.pushButton.setTitle("Push "+nbCommits)
        
    def currentFontChanged(self, info):
        self.font = info['font']
        head, tail = os.path.split(self.font.path)
        self.path = head
        self.fontName = self.font.info.familyName + '-' + self.font.info.styleName
        self.updateTitle()
        if not self.isInGitRepository(self.path):
            self.gitRepo = False
            self.stopSaveObserver(None)
            self.w.pushButton.show(False)
        else:
            self.gitRepo = True
            self.getNumberOfUnpushedCommits(self.font)
        self.updateTitle()
        
    def newFontOpened(self, font):
        for font in AllFonts():
            fontName = font.info.familyName + '-' + font.info.styleName
            head, tail = os.path.split(font.path)
            if self.isInGitRepository(head):
                self.gitRepo = True
                self.commitsCount[fontName] = self.getNumberOfUnpushedCommits(font)
            else:
                self.gitRepo = False
                self.commitsCount[fontName] = 0
                self.w.pushButton.show(False)
        
    def startSaveObserver(self, sender):
        if not self.isInGitRepository(self.path):
            self.gitRepo = False
            return
        self.w.startButton.show(False)
        self.w.stopButton.show(True)
        addObserver(self, "commitWhenSaved", "fontDidSave")
        
    def stopSaveObserver(self, sender):
        self.w.startButton.show(True)
        self.w.stopButton.show(False)
        removeObserver(self, "fontDidSave")
        
    def closeWindow(self, sender):
        try:
            removeObserver(self, "fontDidSave")
        except:
            pass
        removeObserver(self, "fontBecameCurrent")
        removeObserver(self, "fontDidOpen")
        self.w.close()
        
    def commitWhenSaved(self, info):
        UFOPath = info['path']
        head, tail = os.path.split(UFOPath)
        self.path = head
        if not self.gitRepo:
            self.stopSaveObserver(None)
            return
        self.font = info['font']
        self.fontName = self.font.info.familyName + '-' + self.font.info.styleName
        timeTag = time.strftime("%d_%b_%Y_%H_%M_%S", time.gmtime())
        comment =  self.fontName + ' @ ' + timeTag
        subprocess.call(['git', 'add', '-f', UFOPath], cwd=self.path)
        subprocess.call(['git', 'commit', '-am', comment ], cwd=self.path)
        self.commitsCount[self.fontName] += 1
        self.w.pushButton.show(self.path!=None and self.font!=None)
        self.w.pushButton.setTitle('Push '+self.fontName)
        self.updateTitle()
        
    def gitPush(self, sender):
        subprocess.call(['git', 'push'], cwd=self.path)
        self.commitsCount[self.fontName] = 0
        self.w.pushButton.show(False)
        self.updateTitle()
        
commitAtSave()